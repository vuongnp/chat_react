import React, { Component } from "react";
import Moment from "react-moment";

import {
  MDBCard,
  MDBCardBody,
  MDBRow,
  MDBCol,
  MDBListGroup,
  MDBListGroupItem,
  MDBBadge,
  MDBIcon,
  MDBBtn,
  MDBScrollbar
} from "mdbreact";
import "./Chat.css";

const URL = "wss://67cb4aa3.ngrok.io/ws";

class ChatPage extends Component {
  constructor() {
    super();
    this.state = {
      friends: [],
      messages: [],
      newMsg: "",
      curFriend: ""
    };
    this.socket = new WebSocket(URL);
  }

  componentDidMount() {
    fetch("https://67cb4aa3.ngrok.io/users", {
      method: "GET",
      headers: {
        Authorization: this.props.token
      }
    })
      .then(res => {
        if (res.status === 422) {
          throw new Error("Validation failed.");
        }
        if (res.status !== 200 && res.status !== 201) {
          console.log("Error!");
          throw new Error("Could not authenticate you!");
        }
        return res.json();
      })
      .then(resData => {
        console.log(resData.data);
        this.setState({
          friends: resData.data
        });
      })
      .catch(err => {
        console.log(err);
      });

    this.socket.onopen = e => {
      var initMsg = {
        type: "init",
        username: this.props.username
      };
      console.log();
      this.socket.send(JSON.stringify(initMsg));
    };

    // this.socket.onmessage = evt => {
    //   // on receiving a message, add it to the list of messages
    //   const message = JSON.parse(evt.data);
    //   // this.setState({ messages: message });
    //   console.log("Check in onmessege: ", message);
    //   // this.addMessage(message);
    // };

    // this.socket.onclose = () => {
    //   console.log("disconnected");
    //   // automatically try to reconnect on connection loss
    //   this.setState({
    //     ws: new WebSocket(URL)
    //   });
    // };
  }

  showList = username => {
    // event.preventDefault();
    console.log("Username:", username);

    var url = "https://67cb4aa3.ngrok.io/conversation?username=" + username;
    fetch(encodeURI(url), {
      method: "GET",
      headers: {
        Authorization: this.props.token
      }
    })
      .then(res => {
        if (res.status === 422) {
          throw new Error("Validation failed.");
        }
        if (res.status !== 200 && res.status !== 201) {
          console.log("Error!");
          throw new Error("Could not authenticate you!");
        }
        return res.json();
      })
      .then(resData => {
        console.log(resData.data);
        this.setState({
          messages: resData.data,
          curFriend: username
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  clearForm() {
    this.setState({
      newMsg: ""
    });
  }

  inputUpdate(e) {
    const name = e.target.name;
    const value = e.target.value;
    console.log("name:", name);
    console.log("value:", value);
    this.setState({
      [name]: value
    });
  }

  newMessage(e) {
    e.preventDefault();
    var obj = {
      text: this.state.newMsg
    };
    console.log("Send messege:", this.state.newMsg);

    var messObj = {
      type: "chat",
      sender: this.props.username,
      receiver: this.state.curFriend,
      content: this.state.newMsg
    };
    this.socket.send(JSON.stringify(messObj));
    // this.showList(this.state.curFriend);
    this.setState({
      messages: [
        ...this.state.messages,
        {
          sender: this.props.username,
          receiver: this.state.curFriend,
          content: this.state.newMsg,
          inserted_at: Math.floor(Date.now() / 1000)
        }
      ]
    });
    this.socket.onmessage = evt => {
      // on receiving a message, add it to the list of messages
      const message = JSON.parse(evt.data);
      console.log("evt.data:", evt.data);
      this.setState({ messages: [...this.state.messages, message] });
      console.log("Check in onmessege: ", message);
      // this.addMessage(message);
    };
    this.clearForm();
  }

  render() {
    return (
      <MDBCard>
        <MDBCardBody>
          <MDBRow className="px-lg-2 px-2">
            <MDBCol md="6" xl="4" className="px-0 mb-2 mb-md-0">
              <h6 className="font-weight-bold mb-3 text-lg-left">List user</h6>
              <div className="white z-depth-1 p-3">
                <MDBListGroup className="friend-list">
                  {this.state.friends.map(friend => (
                    <Friend
                      key={friend.username}
                      friend={friend}
                      onChange={this.showList.bind(this, friend.username)}
                      token={this.props.token}
                    />
                  ))}
                </MDBListGroup>
              </div>
            </MDBCol>
            <MDBCol md="6" xl="8" className="pl-md-3 px-lg-auto mt-2 mt-md-0">
              <h6 class="font-weight-bold mb-3 text-lg-left">&nbsp;</h6>
              <MDBRow>
                <MDBListGroup className="list-unstyled pl-3">
                  {this.state.messages.length == 0 && (
                    <li className="chat-message d-flex justify-content-between mb-4">
                      <MDBCard>
                        <MDBCardBody>
                          <div>
                            <strong className="primary-font">
                              Welcome to chat
                            </strong>
                          </div>
                          <hr />
                          <p className="mb-0">
                            Choose one person that you want to chat with
                          </p>
                        </MDBCardBody>
                      </MDBCard>
                    </li>
                  )}
                  {this.state.messages.length != 0 &&
                    this.state.messages.map(message => (
                      <ChatMessage
                        key={message.sender + message.inserted_at}
                        message={message}
                      />
                    ))}

                  <li>
                    <form onSubmit={e => this.newMessage(e)}>
                      <div className="form-group basic-textarea">
                        <input
                          className="form-control pl-2 my-0"
                          id="newMsg"
                          name="newMsg"
                          placeholder="Type your message..."
                          autoComplete="off"
                          value={this.state.newMsg}
                          onChange={this.inputUpdate.bind(this)}
                        />
                        <MDBBtn
                          rounded
                          size="bg"
                          className="float-right mt-4"
                          color="secondary"
                          type="submit"
                        >
                          Send
                        </MDBBtn>
                      </div>
                    </form>
                  </li>
                </MDBListGroup>
              </MDBRow>
            </MDBCol>
          </MDBRow>
        </MDBCardBody>
      </MDBCard>
    );
  }
}

const Friend = props => (
  <MDBListGroupItem
    className="d-flex justify-content-between p-2 border-light"
    // style={{ backgroundColor: active ? "#eeeeee" : "" }}
  >
    <MDBBtn color="secondary" rounded onClick={props.onChange}>
      <strong>{props.friend.username}</strong>
    </MDBBtn>
  </MDBListGroupItem>
);

const ChatMessage = ({
  message: { sender, receiver, content, inserted_at }
}) => (
  <li className="chat-message d-flex justify-content-between mb-4">
    <MDBCard>
      <MDBCardBody>
        <div>
          <h6 className="h6" style={{ fontWeight: 700 }}>
            {sender}
          </h6>
          <small className="pull-right text-muted">
            {/* <i className="far fa-clock" /> {} */}
            <Moment>{inserted_at}</Moment>
          </small>
        </div>
        <hr />
        <p className="mb-0">{content}</p>
      </MDBCardBody>
    </MDBCard>
  </li>
);

export default ChatPage;
